package com.company.form.category;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CategoryForCreateForm {
//	private Integer food_and_beverage_id;
	@NotBlank
	private String name;
	@NotBlank
	private String description;
}
