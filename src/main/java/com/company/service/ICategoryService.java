package com.company.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.company.dto.category.CategoryDTO;
import com.company.entity.Category;
import com.company.form.category.CategoryFilterForm;
import com.company.form.category.CategoryForCreateForm;

public interface ICategoryService {

	Page<CategoryDTO> getAllCategories(Pageable pageable, CategoryFilterForm filterForm);

	void createCategory(CategoryForCreateForm form);

	void deleteCategory(Integer id);
}
