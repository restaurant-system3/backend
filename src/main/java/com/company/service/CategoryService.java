package com.company.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.internal.bytebuddy.description.type.TypeVariableToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.company.dto.category.CategoryDTO;
import com.company.entity.Category;
import com.company.form.category.CategoryFilterForm;
import com.company.form.category.CategoryForCreateForm;
import com.company.repository.ICategoryRepository;
import com.company.specification.category.CategorySpecification;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class CategoryService extends BaseService implements ICategoryService {

	@Autowired
	private ICategoryRepository categoryRepository;

	
	@Override
	public Page<CategoryDTO> getAllCategories(Pageable pageable, CategoryFilterForm filterForm) {
		Specification<Category> where = CategorySpecification.buildWhere(filterForm);
		// get entity page
		Page<Category> entityPage = categoryRepository.findAll(where, pageable);
		// convert entity to dto page
		Page<CategoryDTO> dtoPage = convertObjectPageToObjectPage(entityPage, pageable, CategoryDTO.class);
		return dtoPage;
	}

	@Override
	public void createCategory(CategoryForCreateForm form) {
		Category category = new Category();
		category.setName(form.getName());
		category.setDescription(form.getDescription());
		
		categoryRepository.save(category);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void deleteCategory(Integer id) {
		Category category = categoryRepository.getById(id);
		categoryRepository.delete(category);
	}

	public boolean isCategoryExistsById(Integer id) {
		return categoryRepository.existsById(id);
	}

}
