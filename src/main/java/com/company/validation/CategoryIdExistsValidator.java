package com.company.validation;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.service.CategoryService;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class CategoryIdExistsValidator implements ConstraintValidator<CategoryIdExists, Integer> {

	@Autowired
	private CategoryService service;

	@Override
	public boolean isValid(Integer id, ConstraintValidatorContext context) {

		if (id == null || id <= 0) {
			return false;
		}

		return service.isCategoryExistsById(id);
	}
}