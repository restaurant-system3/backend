package com.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.dto.category.CategoryDTO;
import com.company.entity.Category;
import com.company.form.category.CategoryFilterForm;
import com.company.form.category.CategoryForCreateForm;
import com.company.service.ICategoryService;
import com.company.validation.CategoryIdExists;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/categories")
@Validated
public class CategoryController {
	
	@Autowired
	private ICategoryService categoryService;
	
	@GetMapping
	public Page<CategoryDTO> getAllCategories(Pageable pageable,@Valid CategoryFilterForm filterForm){
		return categoryService.getAllCategories(pageable,filterForm);
	}
	
	@PostMapping
	public String createCategory(@RequestBody @Valid CategoryForCreateForm form) {
		categoryService.createCategory(form);
		return "Create Successfully";
	}
	
	@DeleteMapping("/{id}")
	public String deleteCategory(@PathVariable @CategoryIdExists Integer id) {
		categoryService.deleteCategory(id);
		return "Delete Successfully";
	}
}
