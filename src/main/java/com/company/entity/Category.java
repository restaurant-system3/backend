package com.company.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "Category")
@Data
@NoArgsConstructor
@SuperBuilder
public class Category extends BaseEntity {
    private String name;
    private String description;
}
